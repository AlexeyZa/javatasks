import com.sun.jmx.remote.internal.ArrayQueue;

import java.util.*;


class Tree implements Iterable {
    private Component[] components = new Component[50];  //Можно добавить динамическое расширение как в ArrayList, но пока достаточно так.
    private int[][] compRelations = new int[50][50];
    private int elementsCount = 0;

    public Component[] getComponents() {
        return components;
    }


    public void addComponent(Component component, Integer relat) {
        components[elementsCount] = component;
        if (elementsCount != relat) compRelations[elementsCount][relat] = 1;
        elementsCount++;
    }

    public boolean exisrChild(Component component) {
        int index = 0;
        for (int i = 0; i < elementsCount; i++) {
            if (component.equals(components[i])) {
                index = i;
                break;
            }
        }
        for (int j = 0; j < elementsCount; j++) {
            if (compRelations[j][index] == 1)
                return true;
        }
        return false;
    }


    public Iterator<Component> iterator() {
        return new Itr();
    }


    class Itr implements Iterator<Component> {
        Stack<Integer> stack = new Stack<Integer>();
        Queue<Integer> queue = new LinkedList<Integer>();
        int cursor = 0;
        int tempCursor = -1;
        int printedCount = 0;

        public boolean hasNext() {
            return printedCount != elementsCount;
        }

        public Component next() {
            return doDfs();    //Для next() оставил по умолчанию перебор в глубину
        }

        public Component nextBfs() {
            return doBfs();    //второй способ для next()
        }

        public void remove() {
            throw new UnsupportedOperationException("Удаление невозможно");
        }

        //Обход в глубину с использованием стека. Т.к. дерево не бинарное, то из-за LIFO ветки
        // обходятся в глубину не строго слева-направо, а сначала левое, потом крайнее правое, потом второе справа и т.д.
        private Component doDfs() {
            if (printedCount != elementsCount) {
                int rootCheck = 0;
                if (tempCursor == -1) {
                    tempCursor = cursor;
                    printedCount++;
                    return components[0];
                } else {
                    tempCursor = cursor;
                    for (int i = 0; i < components.length; i++) {
                        if (compRelations[i][tempCursor] == 1) {
                            if (rootCheck == 0) {
                                cursor = i;
                                rootCheck = 1;
                            } else stack.push(i);
                        }
                    }
                    if (rootCheck == 0) {
                        if (!stack.isEmpty()) cursor = stack.pop();
                    }
                    printedCount++;
                    return components[cursor];
                }
            } else {
                throw new NoSuchElementException();
            }
        }

        //Обход в глубину с использованием очереди.
        private Component doBfs() {
            if (printedCount != elementsCount) {
                if (tempCursor == -1) {
                    tempCursor = cursor;
                    printedCount++;
                    return components[0];
                } else {
                    tempCursor = cursor;
                    for (int i = 0; i < components.length; i++) {
                        if (compRelations[i][tempCursor] == 1) {
                            queue.offer(i);
                        }
                    }
                    if (!queue.isEmpty()) cursor = queue.poll();
                    printedCount++;
                    return components[cursor];
                }
            } else {
                throw new NoSuchElementException();
            }
        }
    }
}
