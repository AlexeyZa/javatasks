import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Branch implements Component {
    
    private int value;

    public Branch(int value){
        this.value = value;
    }

    public void print() {
        System.out.println("Branch, value "+value);
    }

}
