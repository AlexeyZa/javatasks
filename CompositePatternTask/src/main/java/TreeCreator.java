public class TreeCreator {


    public Tree createTestTree() {
        /*
        Т.к. тема патерны и упоминается про instanceOf, то делаю по патерну composite,
        причем внутри вершины может быть как контейнер(ветка), так и элемент(лист).
        Ветка без потомков может быть только листом.

        Дерево заполняю так: создаю сначала ветки типа component, потом строю древовидную структуру, дополнительно добавляя листы
        Фото структуры приложу в ресурсы для наглядности (надеюсь, не забуду)
        */

        Tree tree = new Tree();

        Component component0 = new Branch(100);
        Component component1 = new Branch(200);
        Component component2 = new Branch(201);
        Component component3 = new Branch(300);
        Component component4 = new Leaf(301);
        Component component5 = new Branch(302);
        Component component6 = new Leaf(55);
        Component component7 = new Leaf(74);
        Component component8 = new Leaf(46);
        Component component9 = new Leaf(27);
        Component component10 = new Leaf(33);
        Component component11 = new Leaf(75);

        tree.addComponent(component0, 0);
        tree.addComponent(component1, 0);
        tree.addComponent(component2, 0);
        tree.addComponent(component3, 1);
        tree.addComponent(component4, 2);
        tree.addComponent(component5, 2);
        tree.addComponent(component6, 0);
        tree.addComponent(component7, 3);
        tree.addComponent(component8, 3);
        tree.addComponent(component9, 1);
        tree.addComponent(component10, 5);
        tree.addComponent(component11, 5);
        return tree;

    }
}
