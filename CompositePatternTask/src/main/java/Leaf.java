public class Leaf implements Component {

    private int value;

    public void print() {
        System.out.println("Leaf, value "+value);
    }

    public Leaf(int value){
        this.value = value;
    }
}
