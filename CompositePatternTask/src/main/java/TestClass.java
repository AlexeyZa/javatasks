import java.util.Iterator;

public class TestClass {

    public static void main (String[] args){
    Tree testTree = new TreeCreator().createTestTree();

        System.out.println("Проход по глубине:");
        Iterator<Component> iterator = testTree.iterator();
        while (iterator.hasNext()) {
            iterator.next().print();
        }
        System.out.println();

        System.out.println("Проход по ширине:");
            Iterator<Component> iterator1 = testTree.iterator();
            while (iterator1.hasNext()){
                ((Tree.Itr)iterator1).nextBfs().print();
            }
        System.out.println();

        new ClientClass().exploreComponents(testTree);


}}
